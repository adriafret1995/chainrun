﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

enum  NumPlayer {P1, P2};

public class P1Controller : MonoBehaviour
{
    [SerializeField]
    private Rigidbody rb;
    [SerializeField]
    private float speed;
    [SerializeField]
    private float jumpSpeed;
    [SerializeField]
    private NumPlayer numberPlayer;

    Vector3 movementDirection;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        PlayerMovement();
    }

    private void PlayerMovement()
    {
        if (numberPlayer == NumPlayer.P1)
        {
            movementDirection = new Vector3(Input.GetAxisRaw("Horizontal1"), 0.0f, Input.GetAxisRaw("Vertical1"));
        }
        else
        {
            movementDirection = new Vector3(Input.GetAxisRaw("Horizontal2"), 0.0f, Input.GetAxisRaw("Vertical2"));
        }


        rb.velocity += movementDirection * speed * Time.deltaTime;
    }
}
